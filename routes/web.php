<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');
Auth::routes();

Route::resource('crud', 'CRUDController')->middleware('auth');
Route::middleware(['throttle:15,1', 'auth'])->group(function () {
    Route::resource('comment', 'CommentController');
    Route::delete('/article/destroy/{id}', 'CommentController@destroy');
});


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/article/{id}', 'CRUDController@show')->middleware('auth');
