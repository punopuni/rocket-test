<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use Auth;
use Illuminate\Support\Facades\Validator;


class CommentController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $post = $request->get('post');
        return view('comment.create', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = $this->validation($request->all());

        if ($validator->passes()) {
            $comment = new Comment([
                'text' => $request->get('text'),
                'user_id' => $request->get('user_id'),
                'post_id' => $request->get('post_id'),
            ]);
            $comment->save();
            return redirect('/article/' . $request->get('post_id'));
        } else {
            return redirect()->back()->withErrors($validator->errors());
        }

    }

    protected function validation(array $data)
    {
        return $validator = Validator::make($data, [
            'text' => 'required',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);
        $post_id = $comment->post_id;
        if ($comment->user_id === Auth::user()->id) {
            $comment->delete();
        }

        return redirect('/article/' . $post_id);
    }


}