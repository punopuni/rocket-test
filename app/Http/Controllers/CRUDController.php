<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;
use Auth;
use Illuminate\Support\Facades\Validator;

class CRUDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cruds = Crud::orderBy('created_at', 'desc')->paginate(5);
        $user_id = Auth::user() ? Auth::user()->id : 0;

        return view('crud.index', compact('cruds', 'user_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('crud.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validation($request->all());
        if($validator->passes()) {
            $crud = new Crud([
                'title' => $request->get('title'),
                'description' => $request->get('description'),
                'content' => $request->get('content'),
                'user_id' => $request->get('user_id'),
            ]);
            $crud->save();
        return redirect('/crud');
        } else {
            return redirect()->back()->withErrors($validator->errors());
        }
    }

     protected function validation(array $data)
    {
        return $validator = Validator::make($data,[
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'content' => 'required',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Crud::find($id);
        $user_id = Auth::user() ? Auth::user()->id : 0;

        return view('crud.article', compact('post','user_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crud = Crud::find($id);

        return view('crud.edit', compact('crud', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $crud = Crud::find($id);
        $crud->title = $request->get('title');
        $crud->description = $request->get('description');
        $crud->content = $request->get('content');
        $crud->save();
        return redirect('/article/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $crud = Crud::find($id);

        $crud->delete();

        return redirect('/crud');
    }

}