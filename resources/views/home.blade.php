@extends('layouts.app')

@section('content')
    <div class="welcome">
        <div class="welcome__wrapper">
            <h1 class="welcome__title">
                Тестовое задание в
                </br>
                <a href="https://rocketfirm.com/" class="welcome__link--by welcome__link" target="_blank">rocket
                    firm</a>
            </h1>
            @if (Auth::check())
                <a href="/crud" class="welcome__link--private welcome__link">
                    К постам
                </a>
            @else
                <div class="welcome__link--wrapper">
                    <a href="/login" class="welcome__link--go welcome__link">
                        Авторизация
                    </a>
                    <a href="/register" class="welcome__link--go welcome__link">
                        Регистрация
                    </a>
                </div>
            @endif
            <a href="https://bitbucket.org/punopuni" class="welcome__link--private welcome__link" target="_blank">
                By Dasha Lev
            </a>
        </div>
    </div>
@endsection
