@extends('layouts.app')
@section('content')
    @include('layouts.header')
    {{--    Скрипты для виджета facebook start--}}
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v6.0"></script>
    {{--    Скрипты для виджета facebook end--}}
    {{--    Скрипты для виджета vk start--}}
    <!-- Put this script tag to the <head> of your page -->
    <script type="text/javascript" src="https://vk.com/js/api/share.js?95"
            charset="windows-1251"></script>
    {{--    Скрипты для виджета vk end--}}
    <section class="main-content">
        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div class="posts">
            <div class="container">
                <div class="post__wrapper">
                    <div class="posts__item--head">
                        <div class="posts__item--name">{{ $post->user->name  }}</div>
                        <div class="posts__item--date">{{$post->created_at->format('Y.m.d')}}</div>
                    </div>
                    <div class="posts__item--title">{{$post->title}}</div>
                    <div class="posts__item--desc">{{$post->description}}</div>
                    <div class="post__item--content">{{$post->content}}</div>
                    <h3>Рассказать в социальных сетях</h3>
                    <div class="post__social">
                        <div class="post__social__item">
                            <div class="fb-share-button" data-href="{{Request::url()}}"
                                 data-layout="button" data-size="small">
                                <a target="_blank"
                                   href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse"
                                   class="fb-xfbml-parse-ignore">Поделиться</a>
                            </div>

                        </div>
                        <div class="post__social__item">
                            <script type="text/javascript"><!--
                                document.write(VK.Share.button(false, {type: "round", text: "Сохранить"}));
                                --></script>
                        </div>
                    </div>
                    <div class="post__button">
                        @if($post->user->id == $user_id)
                            <a href="{{action('CRUDController@edit', $post['id'])}}"
                               class="action-button small">Редактировать текст</a>
                        @endif
                        @if($post->user->id == $user_id)
                            <form action="{{action('CRUDController@destroy', $post['id'])}}" method="post">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="action-button red small" type="submit">Удалить</button>
                            </form>
                        @endif
                    </div>
                </div>
                <div class="post__add--comment">
                    @if($user_id)
                        <a href="{{action('CommentController@create', ['post'=>$post['id']])}}"
                           class="action-button">Оставить комментарий</a>
                    @endif
                </div>
                @if($post->comment && $post->comment->count())
                    <h1 style="color: #fff;">Комментарии</h1>
                    @foreach($post->comment as $comment)
                        <div class="post__wrapper comment__wrapper">
                            <div class="posts__item--head">
                                <div class="posts__item--name">{{ $comment->user->name  }}</div>
                                <div class="posts__item--date">{{$comment->created_at->format('Y.m.d')}}</div>
                            </div>
                            <div class="post__item--content">{{$comment->text}}</div>
                            @if($comment->user->id == $user_id)
                                <form class="comment__btn"
                                      action="{{action('CommentController@destroy', $comment->id)}}"
                                      method="post">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="action-button red small" type="submit">Удалить комментарий</button>
                                </form>
                            @endif
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
@endsection
