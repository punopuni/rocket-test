@extends('layouts.app')
@section('content')
    @include('layouts.header')
    <section class="main-content">
        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div class="posts">
            <div class="container">
                <form method="post" action="{{url('crud')}}" class="posts__forms">
                    <div class="form-group">
                        {{csrf_field()}}
                        <label for="title">Заголовок</label>
                        <input type="text" class="form-control" id="title"
                               placeholder="Заголовок" max-width='255' name="title" required='required'>
                        @if($errors->get('title'))
                            @foreach($errors->get('title') as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <input type="text" class="form-control" id="description"
                               placeholder="Описание"
                               name="description" required='required' max-width='255'>
                        @if($errors->get('description'))
                            @foreach($errors->get('description') as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="content">Текст</label>
                        <textarea name="content" class="form-control" rows="8" cols="80" required='required'></textarea>
                        @if($errors->get('content'))
                            @foreach($errors->get('content') as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group hidden">
                        <label for="user_id" class="col-sm-2 col-form-label col-form-label-lg">Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control form-control-lg " id="user_id"
                                   placeholder="user_id"
                                   name="user_id" value="{{  Auth::user()->id  }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="action-button">
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection