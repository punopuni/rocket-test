@extends('layouts.app')
@section('content')
    @include('layouts.header')
    <section class="main-content">
        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div class="posts">
            <div class="container">
                <form method="post" action="{{action('CRUDController@update', $id)}}" class="posts__forms">
                    <div class="form-group">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PATCH">
                        <label for="title">Заголовок</label>
                        <input type="text" class="form-control" id="title" placeholder="title"
                               name="title"
                               value="{{$crud->title}}">
                    </div>
                    <div class="form-group">
                        <label for="description">Описание</label>
                        <input type="text" class="form-control" id="description"
                               placeholder="description"
                               name="description"
                               value="{{$crud->description}}">
                    </div>
                    <div class="form-group">
                        <label for="content">Текст</label>
                        <textarea name="content" class="form-control" rows="8" cols="80">{{$crud->content}}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="action-button">Обновить</input>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection



