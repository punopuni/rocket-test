@extends('layouts.app')
@section('content')
    @include('layouts.header')
    <section class="main-content">
        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div class="posts">
            <div class="container">

                @if (Auth::check())
                    <div class="posts__add">
                        <a href="/crud/create" class="action-button">Создать пост</a>
                    </div>
                @else
                    <p>Пожалуйста, авторизируйтесь.</p>
                @endif
                @if(isset($cruds))
                    <div class="posts__wrapper">
                        @foreach($cruds as $post)
                            <a href="/article/{{$post->id}}" class="posts__item">
                                <div class="posts__item--head">
                                    <div class="posts__item--name">{{ $post->user->name  }}</div>
                                    <div class="posts__item--date">{{$post->created_at->format('Y.m.d')}}</div>
                                </div>
                                <div class="posts__item--title">{{$post->title}}</div>
                                <div class="posts__item--desc">{{$post->description}}</div>
                            </a>
                        @endforeach
                    </div>
                    {!! $cruds->render() !!}
                @endif
            </div>
        </div>
    </section>
@endsection


