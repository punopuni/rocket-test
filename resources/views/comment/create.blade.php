@extends('layouts.app')
@section('content')
    @include('layouts.header')
    <section class="main-content">
        <ul class="bg-bubbles">
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <div class="posts">
            <div class="container">
                <form method="post" action="{{url('comment')}}" class='posts__forms'>
                    <div class="form-group">
                        {{csrf_field()}}
                        <label for="content">Комментарий</label>
                        <textarea name="text" rows="8" cols="80" required='required' class="form-control"></textarea>
                        @if($errors->get('text'))
                            @foreach($errors->get('text') as $error)
                                <p>{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group  hidden">
                        <label for="user_id">Title</label>
                        <input type="text" class="form-control form-control-lg " id="user_id" placeholder="user_id"
                               name="user_id" value="{{  Auth::user()->id  }}">
                    </div>
                    <div class="form-group  hidden">
                        <label for="user_id">Title</label>
                        <input type="text" class="form-control form-control-lg " id="post_id" placeholder="post_id"
                               name="post_id" value="{{  $post  }}">
                    </div>
                    <div class="form-group">
                        <input type="submit" class="action-button">
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection