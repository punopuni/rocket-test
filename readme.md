# Тестовое задание для Rocket-firm — реализация [блога](https://rocket-test.000webhostapp.com/) (CRUD);

## Описание проекта: 

> Система блога имеет: 
>
> * модули авторизации и регистрации - взяты готовые из Laravel 5.4
>
> * вывод постов;
>
> * возможность добавлять посты *только для зарегистрированных пользователей*;
>
> * возможность редактировать и удалять только свои посты;
> 
> * модуль комментариев с возможностью удаления своего комментария *только автором*;
>
> * защита от спама комментариев с помощью [throttle](https://laravel.com/docs/5.8/middleware#middleware-groups);
>
> * пагинацию, шеринг постов в соцсети VK и Facebook;

## Верстка:

* Кроссбраузерность. Нет поддержки только в IE.
* Адаптивность :)

## Инструменты

- [PHP 7.3](https://www.php.net/);
- [MySql 5.7](https://www.mysql.com/);
- IDE [PhpStrom](https://www.jetbrains.com/ru-ru/phpstorm/);
- [GitHub Desktop](https://desktop.github.com/);
- Панель управления сервером [open server](https://ospanel.io/).
- OS [Windows](https://www.microsoft.com/ru-ru/windows/).
- [Laravel 5.4](https://laravel.com/);
- А так же [SCSS](https://sass-scss.ru/guide/), [webpack](https://webpack.js.org/);

## Как развернуть локально:

   1. Клоним проект на свой компьютер с помощью Git и команды [git clone](https://git-scm.com/docs/git-clone). Места клонирования могут отличаться в зависимости от выбранного сервера;
   2. Скачиваем [composer](https://getcomposer.org/) по инструкции: [composer install](https://getcomposer.org/download/);
   3. Скачиваем необходимые версии PHP, MySql в зависимости от выбранного сервера. Где-то уже сразу можно выбрать необходимую версию.
   4. Заходим в папку с проектом через консоль, и запускаем локальный сервер `php artisan serve`;
***
